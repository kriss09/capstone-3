import {Container} from 'react-bootstrap';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {

	const data = {
		title : "BRAND STORE",
		content : "CLICK AND BUY anywhere, everywhere",
		destination: "/products/",
		label: "BUY NOW!"
	}
	return (
		<>
			<Container>
              	<Banner data={data}/>
              	<Highlights/>
            </Container>
		</>
	)
}
