import { Card, Button, Row, Col, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

  const { _id, name, description, price } = productProp;

  return (

    <Container className="mt-3" xs={1} md={2}>
      <Row>
        <Col lg={{ span: 6, offset: 3}}>
          <Card border="dark">
            <Card.Body className="text-center">
                  <Card.Title className="mb-3" id="card-title">{name}</Card.Title>
                  <Card.Subtitle id="card-subtitle">Description</Card.Subtitle>
                  <Card.Text>{description}</Card.Text>
                  <Card.Subtitle id="card-subtitle">Price</Card.Subtitle>
                  <Card.Text>Php {price}</Card.Text>
                  <Button as={Link} variant="primary" to={`/products/${_id}`}>Details</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>      
  )
}
